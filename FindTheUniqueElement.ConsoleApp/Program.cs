﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindTheUniqueElement.ConsoleApp
{
    class Program
    {
        static string StrResult = "EVET";
        static List<int> result = new List<int>();

        static void Main(string[] args)
        {
            while (StrResult == "EVET")
            {
                int[] nums = new int[600];


                nums = FillArray(nums); //dizimizi random sayılar ile dolduruyoruz.
                FindSingleNumber(nums); //dizimizdeki sayılardan bir adet olanları buluyoruz.


                if (result.Count == 0)
                {
                    Console.Write("Dizideki tüm sayılar 2 adetten fazla bulunmaktadır.\nDizideki sayıları tekrardan üretmek istiyor musunuz?(EVET/HAYIR):");
                    StrResult = Console.ReadLine().ToUpper();
                }
                else
                {
                    result = SortNumber(result); //Sıralanmış sayıları alıyoruz.
                    PrintNumbers(result); //Sayılarımızı ekrana basıyoruz.
                }
                result.Clear();
            }

        }

        private static int[] FillArray(int[] nums)
        {
            int bigNum = 100;
            Random rnd = new Random();

            for (int l = 0; l < nums.Length; l++)
            {
                nums[l] = rnd.Next(bigNum);
            }

            return nums;
        }

        private static void FindSingleNumber(int[] nums)
        {//Random üretilen sayılardan sadece 1 tane olanları buluyoruz.
            int count = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = 0; j < nums.Length; j++)
                {
                    if (nums[i] == nums[j])
                    {
                        count++;
                        if (count >= 2)
                        {
                            break;
                        }
                    }
                }
                if (count == 1)
                {
                    result.Add(nums[i]);
                }
                count = 0;
            }
        }

        private static List<int> SortNumber(List<int> result)
        {
            result.Sort();

            return result;
        }

        private static void PrintNumbers(List<int> result)
        {

            Console.Write("Dizide 1 adet bulunan sayılar:");
            for (int m = 0; m < result.Count; m++)
            {
                if (m < result.Count - 1)
                {
                    Console.Write(result[m] + ",");
                }
                else
                {
                    Console.Write(result[m]);
                }
            }
            Console.Write("\nDizideki sayıları tekrardan üretmek istiyor musunuz?(Evet/Hayır):");
            StrResult = Console.ReadLine().ToUpper();
        }
    }

}
